const httpOrHttps = process.env.VUE_APP_HTTP_OR_HTTPS === undefined ? 'http' : process.env.VUE_APP_HTTP_OR_HTTPS;
const attachmentManagerUrl = process.env.VUE_APP_ATTACHMENT_MANAGER_URL === undefined ? 'localhost:8310' : process.env.VUE_APP_ATTACHMENT_MANAGER_URL;
const publicationManagerUrl = process.env.VUE_APP_PUBLICATION_MANAGER_URL === undefined ? 'localhost:8081' : process.env.VUE_APP_PUBLICATION_MANAGER_URL;
const usersManagerUrl = process.env.VUE_APP_USERS_MANAGER_URL === undefined ? 'localhost:8200' : process.env.VUE_APP_USERS_MANAGER_URL;

module.exports = {
    publicationManagerUrl,
    attachmentManagerUrl,
    usersManagerUrl,
    httpOrHttps
}