# STEP 1 build
FROM node:11-alpine as build-stage
WORKDIR /app
ARG PUBLICATION_MANAGER_URL
ENV VUE_APP_PUBLICATION_MANAGER_URL ${PUBLICATION_MANAGER_URL}
ARG HTTP_OR_HTTPS
ENV VUE_APP_HTTP_OR_HTTPS ${HTTP_OR_HTTPS:-http}
ARG USERS_MANAGER_URL
ENV VUE_APP_USERS_MANAGER_URL ${USERS_MANAGER_URL}
ARG ATTACHMENT_MANAGER_URL
ENV VUE_APP_ATTACHMENT_MANAGER_URL ${ATTACHMENT_MANAGER_URL}
COPY package*.json ./
RUN npm install
COPY . .
RUN npm run build

# STEP 2 deploy
FROM nginx:1.15-alpine
COPY --from=build-stage /app/dist /usr/share/nginx/html
RUN rm /etc/nginx/conf.d/default.conf
COPY nginx/nginx.conf /etc/nginx/conf.d
EXPOSE 8080
CMD ["nginx", "-g", "daemon off;"]
