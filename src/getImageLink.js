import env from "../config/environment-variable";
import VueCookies from "vue-cookies";

export default function getImageLink(attachmentId) {
    return new Promise((resolve, reject) => {
        fetch(env.httpOrHttps +"://" +env.attachmentManagerUrl +"/images/" +attachmentId,{
            method: "GET",
            headers: {
                Authorization: "Bearer " + VueCookies.get("vue-token")
            }
        })
            .then((res)=> {
                res.json()
                    .then((resJson)=>{
                        getLinkImg(resJson.Body.data)
                            .then((linkRes)=>{
                                resolve(linkRes)
                            })
                            .catch((errLinkRes)=>{reject(errLinkRes)})
                    })
                    .catch((errJson)=>{reject(errJson)})
            })
            .catch((err)=> reject(err));
    });
}

async function getLinkImg(buffer) {
    return new Promise((resolve,reject)=>{
        var arrayBufferView = new Uint8Array(buffer);
        var blob = new Blob([arrayBufferView], { type: "image/jpeg" });
        var urlCreator = window.URL || window.webkitURL;
        resolve(urlCreator.createObjectURL(blob));
    });
}