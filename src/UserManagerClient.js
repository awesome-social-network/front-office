import env from "../config/environment-variable";
const rp = require('request-promise');

export default function getUser(token, userId) {
    return new Promise((resolve, reject) => {
        var options = {
            method: 'GET',
            uri: env.httpOrHttps + "://" + env.usersManagerUrl + "/users/" + userId,
            headers: {
                'Authorization': 'Bearer ' + token
            },
            json: true // Automatically stringifies the body to JSON
        }
        rp(options)
            .then(function (result) {
                resolve(result)
            })
            .catch(function (err) {
                reject(err)
            });
    })
}