import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'
import 'bootstrap'
import 'bootstrap/dist/js/bootstrap.min.js'
import 'bootstrap/dist/css/bootstrap.min.css'
import LoginRegistration from "./components/LoginRegister"
import Home from './components/Home.vue'
import Timeline from './components/Timeline.vue'
import EditUser from './components/EditUser.vue'
import UsersList from './components/UsersList.vue'
import UserProfile from './components/UserProfile.vue'
import PageNotFound from './components/PageNotFound'
import Register from './components/Register'
import Help from './components/Help'
import moment from 'moment'
import VueLogger from 'vuejs-logger';
import VModal from 'vue-js-modal';
const isProduction = process.env.NODE_ENV === 'production';

const loggerOptions = {
  isEnabled: true,
  logLevel : isProduction ? 'error' : 'debug',
  stringifyArguments : false,
  showLogLevel : true,
  showMethodName : true,
  separator: '|',
  showConsoleColors: true
};


Vue.prototype.moment = moment
Vue.config.productionTip = false
Vue.use(VueRouter)
Vue.use(VueLogger, loggerOptions)
Vue.use(VModal)

 export const router = new VueRouter({
  mode: 'history',
  routes: [
    { path: '/', component: Home },
    { path: '/login', component: LoginRegistration},
    { path: '/timeline', component: Timeline },
    { path: '/edit', component: EditUser },
    { path: '/users', component: UsersList },
    { path: '/profile', component: UserProfile},
    { path: '/register', component: Register},
    { path: '/help', component: Help },
    { path: "*", component: PageNotFound }
  ]
})

function onKonamiCode(cb) {
  var input = '';
  var key = '38384040373937396665';
  document.addEventListener('keydown', function (e) {
    input += ("" + e.keyCode);
    if (input === key) {
      return cb();
    }
    if (!key.indexOf(input)) return;
    input = ("" + e.keyCode);
  });
}

onKonamiCode(function () {window.location.replace('https://www.youtube.com/watch?v=aMtE4ucwG-8&feature=youtu.be&t=40')})

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
